var Sequelize = require('sequelize');

var sequelize = new Sequelize('foorumi', '', '', {
  dialect: 'sqlite',
  //storage: '/mnt/sdc/git/foorumi-api/db/database.sqlite'
  storage: 'db/database.sqlite'
});

module.exports = {
  DataTypes: Sequelize,
  sequelize: sequelize
};
