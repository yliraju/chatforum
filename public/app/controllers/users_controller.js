FoorumApp.controller('UsersController', function($scope, $location, Api){
  // Toteuta kontrolleri tähän
  $scope.logIn = function(logInUser){
    Api.login(logInUser).success(user => {
      console.log('kirjautuminen onnistui!');
      console.log(user);
      $location.url('/');
    }).error(() => {
      console.log('kirjautuminen epäonnistui');
      $scope.loginErrorMessage = 'Väärä käyttäjätunnus tai salasana!';
    });
  }

  $scope.register = function(newUser) {
    Api.register(newUser).success(user => {
      console.log('rekisteröinti onnistui!');
      console.log(user);
      $location.url('/');
    }).error(() => {
      console.log('rekisteröinti epäonnistui');
      $scope.registerErrorMessage = 'Käyttäjätunnus on jo käytössä!';
    });
  }

});
