FoorumApp.controller('ShowMessageController', function($scope, $routeParams, Api){
  // Toteuta kontrolleri tähän
  Api.getMessage($routeParams.id).success(function(message) {
    $scope.message = message;
  });

  $scope.addReply = function(newReply) {

    let apu = {};
    angular.copy(newReply, apu);
    angular.copy({}, newReply);

    Api.addReply(apu, $routeParams.id).success(function(reply) {
      $scope.message.Replies.push(reply);
    });
  }
});
