FoorumApp.controller('ShowTopicController', function($scope, $routeParams, $location, Api){
  // Toteuta kontrolleri tähän

  $scope.$on('$routeChangeSuccess', function() {

    Api.getTopic($routeParams.id).success(function(topic){
      $scope.topic = topic;
    });

    $scope.addMessage = function(newMessage) {
      Api.addMessage(newMessage, $routeParams.id)
      .success(function(message){
        $location.url('/messages/' + message.id);
      });
    };

  });

});
