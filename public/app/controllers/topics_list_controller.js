FoorumApp.controller('TopicsListController', function($scope, $location, Api){
  // Toteuta kontrolleri tähän

    Api.getTopics().success(function(topics){
        $scope.topics = topics;
    });

    $scope.addTopic = function(newTopic) {
      Api.addTopic({
        name: newTopic.name,
        description: newTopic.description
      })  // addTopic palauttaa lisätyn aiheen, siitä syystä
          // function(topic)
      .success(function(topic){
        $location.url('/topics/' + topic.id);
      });
    }

});
