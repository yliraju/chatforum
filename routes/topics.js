var express = require('express');
var router = express.Router();

var authentication = require('../utils/authentication');
var Models = require('../models');

// Huom! Kaikki polut alkavat polulla /topics

// GET /topics
router.get('/', function(req, res, next) {
    // Hae kaikki aihealueet tässä (Vinkki: findAll)
    Models.Topic.findAll().then(function(topics) {
      if (topics.length === 0) {
        res.sendStatus(204);
      } else {
        res.json(topics);
      }
    });
});

// GET /topics/:id
router.get('/:id', function(req, res, next) {
  // Hae aihealue tällä id:llä tässä (Vinkki: findOne)
  var topicId = req.params.id;
  Models.Topic.findOne({
    where: {
      id: topicId
    },
    include: {
      model: Models.Message,
      include: {
        model: Models.User
      }
    }
  }).then(topic => {
    if (topic === null) {
      res.sendStatus(204);
    } else {
      res.json(topic);
    }
  });
});

// POST /topics
router.post('/', authentication, function(req, res, next) {
  // Lisää tämä aihealue
  var topicToAdd = req.body;
  // Palauta vastauksena lisätty aihealue

  Models.Topic.create(topicToAdd).then(function(topic){
    console.log('aihealue \'' + topicToAdd.name + '\' lisätty onnistuneesti.');
    res.json(topic);
  });

});


// POST /topics/:id/message
router.post('/:id/message', authentication, function(req, res, next) {
  // Lisää tällä id:llä varustettuun aihealueeseen...
  var topicId = req.params.id;
  // ...tämä viesti (Vinkki: lisää ensin messageToAdd-objektiin kenttä TopicId,
  // jonka arvo on topicId-muuttujan arvo ja käytä sen jälkeen create-funktiota)
  var messageToAdd = req.body;
  messageToAdd.TopicId = topicId;
  messageToAdd.UserId = req.session.userId;
  console.log('----');
  console.log(JSON.stringify(messageToAdd));
  console.log('----');
  Models.Message.create(messageToAdd).then(function(message){
    console.log('api/topics.js.post(id/message)');
    console.log(JSON.stringify(message));
    res.json(message);
  })
  .catch(function(err) {
    console.log('error post(/topics/:id/message):');
    console.log(err);
  });
  // Palauta vastauksena lisätty viesti

});

module.exports = router;
